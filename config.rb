set :css_dir, 'stylesheets'

set :js_dir, 'javascripts'

set :images_dir, 'images'

configure :build do
  set :build_dir, 'public'
  set :base_url, "/middleman"
  activate :relative_assets
end
